/**
 * Every programming language has the infamous hello world program everyone learns first.
 * In Node it's quite simple and pretty much just a console.log as most of you might know
 * from Browser JS development.
 */

/**
 * always be careful with declaring variables in Node.js. When the let/const keyword is omitted
 * the variable will result in the global scope and will be available until the node
 * script dies. (To many global variables result in high memory usage and pretty much memory leaks
 * since they can't be gargabe collected
 */
const hello = "Hello World!";

console.log(hello);