export default class Shouter {
    constructor() {
        this.listeners = [];
    }

    addListener(listener) {
        this.listeners.push(listener);
    }

    shout(shout) {
        console.log(`Shouter just shouted '${shout}'`);

        this.listeners.forEach((listener) => {
            listener.onShout(shout);
        });
    }
}