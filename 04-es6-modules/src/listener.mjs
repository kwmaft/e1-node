export default class Listener {
    onShout(shout) {
        console.log(`Listener heard something. Sounded like '${shout}'`);
    }
}