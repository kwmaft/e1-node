/**
 * Export and import is not limited to modules.exports and require any more
 * since Node.js v8.5. Since the Version 8.5 Node.js also supports ES6 modules.
 *
 * This feature currently is still experimental, but will be included in the core in
 * future versions.
 *
 * ES6 Modules need the extension .mjs instead of .js and when running the script
 * the flag --experimental-modules needs to be used.
 *
 * > node --experimental-modules app.js
 */
import Shouter from "./src/shouter";
import Listener from "./src/listener";

const shouter = new Shouter();
const listener = new Listener();

shouter.addListener(listener);
shouter.shout("KWM is currently rocking Node.js!");