/**
 * Modules defined in the package.json can be installed with npm install.
 * This will install any npm module which is defined within dependencies
 * and devDependencies.
 *
 * NPM modules can be required by their given name. The module lodash is installed
 * via the name lodash and the source is available in node_modules/lodash.
 */
const _ = require("lodash");

/**
 * Lodash is a library with many helper functions a lot of other modules depend on.
 * It's available in almost any Node.js application.
 *
 * @see https://lodash.com/
 */

console.log("[] is an array = " + _.isArray([]));

console.log();
console.log("-----------------------");
console.log("_.forEach with an array");
_.forEach(["a", "b", "c"], (element) => {
    console.log(element);
});

console.log();
console.log("-----------------------");
console.log("_.forEach with an object");
_.forEach({a: "a", b: "b", c: "c"}, (value, key) => {
    console.log(key + " => " + value);
});

/**
 * Many other neat helpers like _.debounce, _.throttle, ....
 */

/**
 * jQuery and Bootstrap can also be installed via NPM (preferred way). This will
 * be useful next week when we take a peak at webpack.
 */
// const $ = require("jquery")