const config = require("../resources/config/config");

class Application {
    constructor() {
        this.name = config.app.name;
    }

    getName() {
        return this.name;
    }
}

/**
 * It is also possible to export multiple classes.
 *
 * module.exports.Application = Application
 * module.exports.AnotherClass = AnotherClass
 *
 * When requiring this file afterwards both classes are available:
 * const SomeName = require(".../application");
 *
 * SomeName.Application
 * SomeName.AnotherClass
 */

module.exports = Application;