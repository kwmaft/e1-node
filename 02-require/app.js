/**
 * Seperate source files can be included with the function require, providing
 * a relative path to the given file.
 *
 * Those files can define what the result of require will be by utilizing the
 * object module.exports which is available within those files.
 *
 * @see application.js and config.js for examples on how to export an object or a class
 * respectively.
 */
const Application = require("./src/application/application");

const app = new Application();

console.log(app.getName());